import React, { Component } from 'react'
import './matrix.style.css'
import MatrixButton from "../matrix-button/matrix-button.component";

class Matrix extends Component {
    constructor(props) {
        super(props);
        this.height = props.height;
        this.width = props.width;
    }

    render() {
        let buttons = [];
        for (let j = 0; j < this.height; j++) {
            let span = [];
            for(let i = 0; i < this.width; i++) {
                span.push(<MatrixButton/>);
            }
            buttons.push(<span>{span}</span>)
        }
        console.log(this.height);
        console.log(this.width);
        return (
            <div className="matrix">
                {buttons}
                <p>{this.props.videoLink}</p>
            </div>
        );
    }
}

export default Matrix;