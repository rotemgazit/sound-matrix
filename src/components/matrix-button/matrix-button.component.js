import React, { Component } from 'react'
import './matrix-button.style.css'

class MatrixButton extends Component {
    constructor(props) {
        super(props);
        this.state = {class: ''};
        this.toggleLight = this.toggleLight.bind(this);
    }

    toggleLight() {
        if(this.state.class.length === 0) {
            console.log('hi')
            this.setState({class: 'on'});
        }
        else {
            this.setState({class: ''});
        }
    }

    render() {
        return (
          <div className={"matrix-button" + " " + this.state.class} onClick={this.toggleLight}></div>
        );
    }
}

export default MatrixButton;